#include "threads.h"
std::mutex mtx;

void I_Love_Threads()
{
	cout << "I Love Threads" << endl;
}

void call_I_Love_Threads()
{
	std::thread t1(I_Love_Threads);
	t1.join();
}

void printVector(vector<int> primes)
{
	for (auto i = primes.begin(); i != primes.end(); ++i)
		cout << *i << endl; // Print value
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	int low = begin , high = end , counter = 0, flag = 0;
	while (low < high)
	{
		flag = 0;

		for (counter = 2; counter <= low / 2; ++counter)
		{
			if (low % counter == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
			primes.push_back(low); // Get into vector
		++low;
	}
}

vector<int> callGetPrimes(int begin, int end)
{
	clock_t t;
	t = clock();
	vector<int> primeVector;
	std::thread primeThread(getPrimes , begin , end , ref(primeVector)); // Call thread
	primeThread.join();
	t = clock() - t;
	cout << "time: " << t << " miliseconds" << endl;
	return primeVector;
}

void writePrimesToFile(int begin, int end, ofstream & file , int threadNumber)
{
	//cout << "Thread number " << threadNumber << endl;
	int low = begin, high = end, counter = 0, flag = 0;
	while (low < high)
	{
		flag = 0;
		for (counter = 2; counter <= low / 2; ++counter)
		{
			if (low % counter == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0)
		{
			mtx.lock();
			file << low << endl;
			mtx.unlock();
		}
		++low;
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	std::thread* threadArr = new std::thread[N]; // Will hold the threads
	int startPlace = 0, finishPlace = 0;
	std::ofstream file(filePath);
	clock_t timer;
	int div = end/N;
	timer = clock();
	for (int i = 0; i < N; i++) // Divide to threads 
	{
		startPlace = finishPlace;
		finishPlace += div;
		//cout << startPlace << " , " << finishPlace << endl;
		threadArr[i]  = thread(writePrimesToFile, startPlace, finishPlace, ref(file) , i);
	}
	for (int i = 0; i < N; i++)
	{
		threadArr[i].join(); // Wait for each thread
	}
	file << "\n\n";
	cout << "Time took: " << clock() - timer << " miliseconds" << endl;
	delete[] threadArr;
}
