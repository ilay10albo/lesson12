#include "threads.h"
#include <fstream>
#include <thread>
#include <string>

int main()
{
	//call_I_Love_Threads();

	vector<int> primes1;
	getPrimes(58, 100, primes1);
	callGetPrimes(0 ,1000);
	callGetPrimes(0, 100000);  //1800 ms  
	callGetPrimes(0, 1000000); // Very slow 

	vector<int> primes3 = callGetPrimes(93, 289);

	callWritePrimesMultipleThreads(0, 1000, "primes2.txt", 2);
	callWritePrimesMultipleThreads(0, 1000000, "primes2.txt", 4);
	
	system("pause");
	return 0;
}

