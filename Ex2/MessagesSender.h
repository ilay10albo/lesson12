#pragma once
#include <thread>
#include <stdio.h>
#include <Windows.h>
#include <iostream>
#include <string>
#include <set>
#include <mutex>
#include <condition_variable>
#include <fstream>
#include <queue>

using namespace std;
using std::queue;

class MessagesSender
{
private:
	// Messages locker
	std::mutex locker;
	std::condition_variable cond;
	
	// Clients locker
	std::mutex simpleClient;
	std::condition_variable userCondition;
	
	bool running = true;

	string _fileName;
	string _outPut;
	ifstream _inputFile;
	ofstream _outFile;

public:
	

	queue<string> _messages;
	set<string> _clients;
	MessagesSender(string _fileName , string _outPut );
	void fileDataCheck();
	void writeToOutput();
	bool isExist(string name);
	void SignIn();
	void SignOut();
	void ShowClients();
	set<string> getVector();
	void ShowWindows();
	~MessagesSender();
};

