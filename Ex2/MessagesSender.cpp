#include "MessagesSender.h"

bool MessagesSender::isExist(string name)
{
	std::set<string>::iterator it;
	for (it = _clients.begin(); it != _clients.end(); ++it)
	{
		if (*it == name)
		{
			return true;
		}
	}
	return false;
}

MessagesSender::MessagesSender(string _fileName , string _outPUT)
{
	this->_fileName = _fileName;
	this->_outPut = _outPut;
	std::thread dataThread(&MessagesSender::fileDataCheck , this);
	std::thread outThread(&MessagesSender::writeToOutput, this);
	string option = "";
	system("CLS");
	while(option != "4")
	{ 
		ShowWindows();
		cin >> option;
		getchar();
		switch (option[0])
		{
		case '1':
			SignIn();
			break;
		case '2':
			SignOut();
			break;
		case '3':
			ShowClients();
			break;
		case '4':
			exit(0);
			break;
		default:
			break;
		}
		system("CLS");
	}
	dataThread.join();
	outThread.join();
}
set<string> MessagesSender::getVector()
{
	return _clients;
}
void MessagesSender::SignIn()
{	
	string name = "";
	system("CLS");
	cout << "Enter Name\n-->";
	cin >> name; getchar();
	if (isExist(name))
	{
		cout << "User: " << name << " is already in the list! " << endl;
		getchar();
		return;
	}
	simpleClient.lock();
	cout << "User " << name << " was inserted :)" << endl;
	_clients.insert(name);
	simpleClient.unlock();
	getchar();
}

void MessagesSender::SignOut()
{
	string name = "";
	system("CLS");
	cout << "Enter Name\n-->";
	cin >> name; getchar();
	if (!isExist(name))
	{
		cout << "User: " << name << " is not in the list! " << endl;
		getchar();
		return;
	}
	cout << "User " << name << " was deleted :)" << endl;
	simpleClient.lock(); 
	_clients.erase(name);
	simpleClient.unlock();
	getchar();
}

void MessagesSender::ShowClients()
{
	int counter = 1;
	system("CLS");
	std::set<string>::iterator it;
	for (it = _clients.begin(); it != _clients.end(); counter++,++it)
		cout << counter << ".	" << *it << endl;
	getchar();
}

void MessagesSender::fileDataCheck()
{
	
	string line;
	
	ifstream myfile(_fileName);
	if (myfile.is_open())
	{
		while (true)
		{

			std::unique_lock<mutex> loci(this->locker);
			while (getline(myfile, line))
			{
				cout << line << '\n';
				
				_messages.push(line); // Critical section
				
			}

			loci.unlock();
			cond.notify_one();

			myfile.close();
			myfile.open(_fileName, std::ifstream::out | std::ifstream::trunc); // Empty file
			myfile.close();
			Sleep(5000); // Five seconds - quicker
			myfile.open(_fileName); // Get new information from admin
		}
	}
	
	else cout << "Unable to open file";
}

void MessagesSender::writeToOutput()
{
	std::string currentLine = "";
	std::set<string> tempClients;
    std::set<string>::iterator it;
	ofstream outfile(_outPut, ios::app);	
	while (true)
	{
		std::unique_lock<mutex> loci(locker);
		cond.wait(loci);
		simpleClient.lock();
		tempClients = _clients;
		simpleClient.unlock();
		if (_clients.size() > 0)
		{			
			while (!_messages.empty())
			{

				currentLine = _messages.front();
				_messages.pop();
				for (it = tempClients.begin(); it != tempClients.end(); ++it)
				{
					outfile << *it + ":" + currentLine + "\n"; // Write current change
				}
			}
			outfile.close();
			outfile.open("output.txt", ios::app);
		}
	}
	
}


void MessagesSender::ShowWindows()
{ 
	std::cout <<"1.	Signin\n2.      Signout\n3.	Connected Users\n4.	exit"<<endl;
}

MessagesSender::~MessagesSender()
{
}
