#pragma once
#include <mutex>
#include <condition_variable>
#include <iostream>
#include <string>
#include <queue>
#include <stack>
#include "MessagesSender.h"

#include <fstream>
#include <Windows.h>
#include <queue>
using namespace std;
using std::queue;

using namespace std;
using std::queue;

class readersWriters{
private:
	
	std::mutex _mu;
	std::unique_lock<std::mutex> _locker;
	std::condition_variable _condW;
	std::condition_variable _condR;
	int _readersNumber;
	int _writersNumber;
	std::string _fileName;
	queue<string> _messages;
	set<string> _clients;
	MessagesSender *a;
public:
	//-----------
	bool isExist(string name);
	void SignIn();
	void SignOut();
	void ShowClients();
	void ShowWindows();

	void fileDataCheck();
	void writeDataToOutputFile();


	readersWriters(std::string fileName);
	void readLock();
	void writeLock();
	void readUnlock();
	void writeUnlock();
	std::string readLine(int lineNumber); //lineNumber - line number to read
	void WriteLine(int lineNumber, std::string newLine);//lineNumber - line number to write 
};


