#include "readersWriters.h"


void readersWriters::fileDataCheck()
{
	
	string line;
	ifstream myfile(this->_fileName);
	if (myfile.is_open())
	{
		while (true)
		{
			while (getline(myfile, line))
			{
				cout << line << '\n';
				_mu.lock();
				_messages.push(line);
				_mu.unlock();
			}
			myfile.close();
			myfile.open(_fileName, std::ifstream::out | std::ifstream::trunc); // Empty file
			myfile.close();
			Sleep(5000); // Five seconds - quicker
			myfile.open(_fileName); // Get new information from admin
		}
	}
	else cout << "Unable to open file";
}

void readersWriters::writeDataToOutputFile()
{
}

readersWriters::readersWriters(std::string fileName )
{

	this->_fileName = fileName;
	string option = "";
	system("CLS");
	while (option != "4")
	{
		ShowWindows();
		cin >> option;
		getchar();
		switch (option[0])
		{
		case '1':
			SignIn();
			break;
		case '2':
			SignOut();
			break;
		case '3':
			ShowClients();
			break;
		case '4':
			break;
		default:
			break;
		}
		system("CLS");
	}
	
}


void readersWriters::readLock()
{
}

void readersWriters::writeLock()
{
}

void readersWriters::readUnlock()
{
}

void readersWriters::writeUnlock()
{
	
}

std::string readersWriters::readLine(int lineNumber)
{
	return std::string();
}

void readersWriters::WriteLine(int lineNumber, std::string newLine)
{
}





bool readersWriters::isExist(string name)
{
	std::set<string>::iterator it;
	for (it = _clients.begin(); it != _clients.end(); ++it)
	{
		if (*it == name)
		{
			return true;
		}
	}
	return false;
}

void readersWriters::SignIn()
{
	string name = "";
	system("CLS");
	cout << "Enter Name\n-->";
	cin >> name; getchar();
	if (isExist(name))
	{
		cout << "User: " << name << " is already in the list! " << endl;
		getchar();
		return;
	}
	cout << "User " << name << " was inserted :)" << endl;
	_clients.insert(name); getchar();
}

void readersWriters::SignOut()
{
	string name = "";
	system("CLS");
	cout << "Enter Name\n-->";
	cin >> name; getchar();
	if (!isExist(name))
	{
		cout << "User: " << name << " is not in the list! " << endl;
		getchar();
		return;
	}
	cout << "User " << name << " was deleted :)" << endl;
	_clients.erase(name); getchar();
}

void readersWriters::ShowClients()
{
	int counter = 1;
	system("CLS");
	std::set<string>::iterator it;
	for (it = _clients.begin(); it != _clients.end(); counter++, ++it)
		cout << counter << ".	" << *it << endl;
	getchar();
}

void readersWriters::ShowWindows()
{
	std::cout << "1.	Signin\n2.      Signout\n3.	Connected Users\n4.	exit" << endl;

}